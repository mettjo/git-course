# git course

![](http://phdcomics.com/comics/archive/phd101212s.gif)

---

## Getting help

If you don't know how to do something, what do you do?

[![](https://git-scm.com/images/progit2.png)](https://git-scm.com/book/en/v2)

[Pro Git book (free online)](https://git-scm.com/book/en/v2)

> You'll probably forget everything you learn today.

Best way to learn is to try and solve your own problem, then if you get stuck, call me for help!

---

## Git state diagram

![](https://d2hmx6toszlvdq.cloudfront.net/2016/02/GitHub-cheat-sheet-graphic-v1.jpg)

---

## Commit messages

![](https://imgs.xkcd.com/comics/git_commit_2x.png)

> Treat commit messages as if they are _commands_ which execute the change.

1. write in an imperative mood (like a command or request); for example:
    * Capitalise word in documentation
    * Add system test to check file creation
    * Remove redundant XYZ parser
2. Capitalise first letter
3. 50-character limit
4. No full-stop at the end

## Branches


https://git-school.github.io/visualizing-git/


## git cheatsheet

[![](https://i.redd.it/8341g68g1v7y.png)](https://i.redd.it/8341g68g1v7y.png)