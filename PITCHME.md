# git course

![](http://phdcomics.com/comics/archive/phd101212s.gif)

---

## Getting help

If you don't know how to do something, what do you do?

[![](https://git-scm.com/images/progit2.png)](https://git-scm.com/book/en/v2)

[Pro Git book (free online)](https://git-scm.com/book/en/v2)

> You'll probably forget everything you learn today.

Best way to learn is to try and solve your own problem, then if you get stuck, call me for help!

---

## Git state diagram

![](https://d2hmx6toszlvdq.cloudfront.net/2016/02/GitHub-cheat-sheet-graphic-v1.jpg)

---

## Commit messages

![](https://imgs.xkcd.com/comics/git_commit_2x.png)

For the following change:


``` diff
diff --git a/filea.extension b/fileb.extension
index d28nd309d..b3nu834uj 111111
--- a/filea.extension
+++ b/fileb.extension
@@ -1,6 +1,6 @@
-oldLine
+newLine
```

**✗ Don't do:**

```shell
$ git commit -m "updated line"
```

**✓ Instead do:**

```shell
$ git commit -m "Capitalise Line in documentation"
```

> Treat commit messages as if they are _commands_ which execute the change.